import { useEffect, useState } from "react";

const InputForm = () => {
    const [firstName, setFirstName] = useState(localStorage.getItem("firstname") || "");
    const [lastName, setLastName] = useState(localStorage.getItem("lastname") || "");

    const onInputFirstnameChangeHandler = (event) => {
        setFirstName(event.target.value);
    }

    const onInputLastnameChangeHandler = (event) => {
        setLastName(event.target.value);
    }

    useEffect(() => {
        localStorage.setItem("firstname", firstName);
        localStorage.setItem("lastname", lastName);
    }, [firstName, lastName])

    return (
        <div style={{marginTop: "100px"}}>
            <input value={firstName} onChange={onInputFirstnameChangeHandler} />
            <br />
            <input value={lastName} onChange={onInputLastnameChangeHandler} />
            <br />
            <p> {lastName} {firstName} </p>
        </div>
    )
}

export default InputForm;